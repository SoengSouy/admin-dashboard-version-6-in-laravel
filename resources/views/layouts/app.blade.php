<!DOCTYPE html>
<html lang="en">
<head>
	<title>soengsouy.com</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="{{URL::to('assets/images/favicon.ico')}}" type="image/x-icon">
	<!-- vendor css -->
	<link rel="stylesheet" href="{{URL::to('assets/css/style.css')}}">
</head>

	@yield('content')

	<!-- Required Js -->
	<script src="{{URL::to('assets/js/vendor-all.min.js')}}"></script>
	<script src="{{URL::to('assets/js/plugins/bootstrap.min.js')}}"></script>
	<script src="{{URL::to('assets/js/ripple.js')}}"></script>
	<script src="{{URL::to('assets/js/pcoded.min.js')}}"></script>

</body>
</html>